// ignore_for_file: import_of_legacy_library_into_null_safe

import 'package:flutter/material.dart';
import 'package:qu_est_ce_que_on_mange/ui/screens/menu/main_home_page.dart';
import 'package:qu_est_ce_que_on_mange/ui/screens/recipe_creation_screen.dart';
import 'package:qu_est_ce_que_on_mange/ui/screens/recipe_detail_screen.dart';
import 'package:qu_est_ce_que_on_mange/ui/screens/recipe_screen.dart';
import 'package:qu_est_ce_que_on_mange/ui/styles/Colors.dart';

import 'database/operations/menuDAO.dart';
import 'models/menu.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  MenuDAO menuDAO = MenuDAO();

  @override
  Widget build(BuildContext context) {
    menuDAO.createMenu(const Menu());
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Qu'est-ce qu'on mange",
      theme: ThemeData(scaffoldBackgroundColor: backgroundColor),
      initialRoute: '/home',
      routes: {
        '/home': (context) => const MenuPage(),
        '/recipe-creation': (context) => const RecipeCreationScreen(),
        '/recipe-detail': (context) => const RecipeDetailScreen(),
      },
    );
  }
}
