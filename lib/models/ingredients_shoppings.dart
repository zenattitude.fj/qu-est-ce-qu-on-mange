class IngredientsShoppings {
  final int id_ingredient;
  final int id_shopping;
  final int quantity;
  final String? unity;

  const IngredientsShoppings(
      {required this.id_ingredient,
      required this.id_shopping,
      required this.quantity,
      this.unity});

  IngredientsShoppings.fromMap(Map<String, dynamic> map)
      : id_ingredient = map['id_ingredient'],
        id_shopping = map['id_shopping'],
        quantity = map['quantity'],
        unity = map['unity'];

  Map<String, dynamic> toMap() => {
        'id_ingredient': id_ingredient,
        'id_shopping': id_shopping,
        'quantity': quantity,
        'unity': unity
      };
}
