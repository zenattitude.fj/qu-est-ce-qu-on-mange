import 'ingredients_shoppings.dart';

class Shopping {
  final int? id;
  final String? name;
  final List<IngredientsShoppings> ingredientsShoppingsList;

  const Shopping(
      {this.id, this.name, this.ingredientsShoppingsList = const []});

  Shopping.fromMap(Map<String, dynamic> map)
      : id = map['id'],
        name = map['name'],
        ingredientsShoppingsList = map.containsKey('ingredientsShoppingsList')
            ? (map['ingredientsShoppingsList'] as List)
                .map((ingredientsShoppings) =>
                    IngredientsShoppings.fromMap(ingredientsShoppings))
                .toList()
            : <IngredientsShoppings>[];

  Map<String, dynamic> toMap() => {
        'name': name,
        'ingredientsShoppingsList': ingredientsShoppingsList
            .map((ingredientsShoppings) => ingredientsShoppings.toMap())
            .toList()
      };
}
