class IngredientsRecipes {
  final int id_ingredient;
  final int? id_recipe;
  final int quantity;
  final String? unity;

  const IngredientsRecipes(
      {required this.id_ingredient,
      this.id_recipe,
      required this.quantity,
      this.unity});

  IngredientsRecipes.fromMap(Map<String, dynamic> map)
      : id_ingredient = map['id_ingredient'],
        id_recipe = map['id_recipe'],
        quantity = map['quantity'],
        unity = map['unity'];

  Map<String, dynamic> toMap() => {
        'id_ingredient': id_ingredient,
        'id_recipe': id_recipe,
        'quantity': quantity,
        'unity': unity
      };

  IngredientsRecipes withIdRecipe(int newId_recipe) {
    return IngredientsRecipes(
        id_ingredient: id_ingredient,
        id_recipe: newId_recipe,
        unity: unity,
        quantity: quantity);
  }
}
