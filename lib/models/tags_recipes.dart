class TagsRecipes {
  final int id_recipe;
  final int id_tag;

  const TagsRecipes({required this.id_recipe, required this.id_tag});

  TagsRecipes.fromMap(Map<String, dynamic> map)
      : id_recipe = map['id_recipe'],
        id_tag = map['id_tag'];

  Map<String, dynamic> toMap() => {
        'id_recipe': id_recipe,
        'id_tag': id_tag,
      };
}
