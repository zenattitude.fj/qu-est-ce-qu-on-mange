class Tag {
  final int? id;
  final String? name;

  const Tag({this.id, this.name});

  Tag.fromMap(Map<String, dynamic> map)
      : id = map['id'],
        name = map['name'];

  Map<String, dynamic> toMap() => {
        'name': name,
      };
}
