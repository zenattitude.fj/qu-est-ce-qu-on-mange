class Ingredient {
  static const String TABLE_NAME = 'ingredient';
  final int? id;
  final String name;
  final String category;

  const Ingredient({this.id, required this.name, required this.category});

  Ingredient.fromMap(Map<String, dynamic> map)
      : id = map['id'],
        name = map['name'],
        category = map['category'];

  Map<String, dynamic> toMap() => {
        'name': name,
        'category': category,
      };

  Ingredient withId(int newId) {
    return Ingredient(id: newId, name: name, category: category);
  }
}
