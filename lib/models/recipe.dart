class Recipe {
  static const String TABLE_NAME = 'recipe';

  final int? id;
  final String? title;
  final String? description;
  final String? image;
  final int? score;
  final int? menuId;

  const Recipe(
      {this.id,
      this.title,
      this.description,
      this.image,
      this.score,
      this.menuId});

  Recipe.fromMap(Map<String, dynamic> map)
      : id = map['id'],
        title = map['title'],
        description = map['description'],
        image = map['image'],
        score = map['score'],
        menuId = map['menuId'];

  Map<String, dynamic> toMap() => {
        'title': title,
        'description': description,
        'image': image,
        'score': score,
        'menuId': menuId
      };
}
