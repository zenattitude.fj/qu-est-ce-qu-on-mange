import 'package:flutter/material.dart';
import 'package:qu_est_ce_que_on_mange/ui/styles/Colors.dart';

Color getBgColor(int index, int _pageIndex) =>
    _pageIndex == index ? primaryColor : secondaryColor;

Widget buildIcon(IconData iconData, int index, int _pageIndex) => SizedBox(
      width: double.infinity,
      height: kBottomNavigationBarHeight,
      child: Material(
        color: getBgColor(index, _pageIndex),
        child: InkWell(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[Icon(iconData)],
          ),
        ),
      ),
    );
