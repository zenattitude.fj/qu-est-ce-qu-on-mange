import 'package:flutter/material.dart';

const IconData recipe_icon = Icons.menu_book_rounded;
const IconData shopping_icon = Icons.shopping_cart_outlined;
const IconData add_icon = Icons.add;
const IconData home_icon = Icons.home;
const IconData validation_icon = Icons.check;
const IconData back_icon = Icons.chevron_left;
