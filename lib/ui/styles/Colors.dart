import 'package:flutter/material.dart';

const Color backgroundColor = Color.fromRGBO(255, 201, 164, 1);
const Color primaryColor = Color.fromRGBO(255, 83, 29, 1);
const Color secondaryColor = Color.fromRGBO(249, 250, 255, 1);
const Color darkPrimaryColor = Color(1);
const Color colorShadow = Color.fromRGBO(0, 0, 0, 0.6);
