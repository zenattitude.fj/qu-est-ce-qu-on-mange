// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:qu_est_ce_que_on_mange/ui/styles/Colors.dart';

import '../../../utils/constants/UIConstants.dart';
import '../../common_widgets/BoxDecorationButton.dart';
import '../home_screen.dart';
import '../recipe_screen.dart';
import '../shopping_screen.dart';
import '../../../utils/HelperUi.dart';

class MenuPage extends StatefulWidget {
  const MenuPage({key});

  @override
  State<MenuPage> createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  IconData _iconLeft = recipe_icon;
  IconData _iconRight = shopping_icon;

  int _pageIndex = 1;

  final List<Widget> _tabList = [
    RecipeScreen(),
    HomeScreen(),
    ShoppingScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          _tabList.elementAt(_pageIndex),
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: Align(
              alignment: Alignment(0.0, 1.0),
              child: Container(
                decoration: const BoxDecorationButton(),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(
                    Radius.circular(30),
                  ),
                  child: BottomNavigationBar(
                    selectedItemColor: secondaryColor,
                    unselectedItemColor: primaryColor,
                    backgroundColor: secondaryColor,
                    showSelectedLabels: false,
                    selectedFontSize: 0,
                    showUnselectedLabels: false,
                    currentIndex: _pageIndex,
                    onTap: onTap,
                    items: [
                      BottomNavigationBarItem(
                          icon: buildIcon(_iconLeft, 0, _pageIndex),
                          label: 'menu'),
                      BottomNavigationBarItem(
                          icon: buildIcon(home_icon, 1, _pageIndex),
                          label: 'home'),
                      BottomNavigationBarItem(
                          icon: buildIcon(_iconRight, 2, _pageIndex),
                          label: 'shopping'),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  void onTap(int index) {
    this.navigate(index);
    setState(() {
      setIcons(index);
      _pageIndex = index;
    });
  }

  void setIcons(int index) {
    switch (index) {
      case 0:
        _iconLeft = add_icon;
        _iconRight = shopping_icon;
        break;
      case 2:
        _iconLeft = recipe_icon;
        _iconRight = add_icon;
        break;
      default:
        _iconLeft = recipe_icon;
        _iconRight = shopping_icon;
        break;
    }
  }

  void navigate(int index) {
    if (index == _pageIndex) {
      if (index == 0) {
        Navigator.pushNamed(context, '/recipe-creation');
      } else if (index == 2) {}
    }
  }
}
