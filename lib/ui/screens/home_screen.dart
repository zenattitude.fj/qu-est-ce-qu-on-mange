import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: const [
          Padding(
            padding: EdgeInsets.all(40),
            child: Center(
                child: Text(
              "Qu'est-ce qu'on mange",
              style: TextStyle(fontSize: 32),
              textAlign: TextAlign.center,
            )),
          )
        ],
      ),
    );
  }
}
