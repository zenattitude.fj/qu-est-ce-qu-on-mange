import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:qu_est_ce_que_on_mange/database/operations/ingredientDAO.dart';
import 'package:qu_est_ce_que_on_mange/models/ingredient.dart';
import 'package:qu_est_ce_que_on_mange/models/ingredients_recipes.dart';
import 'package:qu_est_ce_que_on_mange/ui/common_widgets/BoxDecorationButton.dart';
import 'package:qu_est_ce_que_on_mange/ui/common_widgets/customDialog.dart';

import '../../utils/HelperUi.dart';
import '../../utils/constants/UIConstants.dart';
import '../styles/Colors.dart';

class RecipeCreationScreen extends StatefulWidget {
  const RecipeCreationScreen({super.key});

  @override
  State<RecipeCreationScreen> createState() => _RecipeCreationScreenState();
}

class _RecipeCreationScreenState extends State<RecipeCreationScreen> {
  IngredientDAO ingredientDAO = IngredientDAO();
  final TextEditingController txtTitle = TextEditingController();
  final TextEditingController txtQuantity = TextEditingController();
  final TextEditingController txtUnity = TextEditingController();
  final TextEditingController txtIngredientName = TextEditingController();
  final TextEditingController txtIngredientcategory = TextEditingController();
  double _rating = 1;
  double _initialRating = 2.0;
  List<IngredientsRecipes> _ingredients = [];
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();
    return Scaffold(
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: recipeCreationPage(txtTitle, _formKey),
          ),
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: Align(
              alignment: const Alignment(0.0, 1.0),
              child: Container(
                decoration: const BoxDecorationButton(),
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(30),
                  ),
                  child: BottomNavigationBar(
                    selectedItemColor: secondaryColor,
                    unselectedItemColor: primaryColor,
                    backgroundColor: secondaryColor,
                    showSelectedLabels: false,
                    selectedFontSize: 0,
                    showUnselectedLabels: false,
                    currentIndex: 1,
                    onTap: (index) {
                      onTap(index, _formKey);
                    },
                    items: [
                      BottomNavigationBarItem(
                          icon: buildIcon(back_icon, 0, 1), label: 'menu'),
                      BottomNavigationBarItem(
                          icon: buildIcon(validation_icon, 1, 1),
                          label: 'home'),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget recipeCreationPage(
      TextEditingController txtTitle, GlobalKey<FormState> _formKey) {
    return Form(
      key: _formKey,
      child: ListView(
        children: [
          titleInput(txtTitle),
          imageInput(),
          scoreInput(),
          ...ingredientsInput(),
          desciptionInput()
        ],
      ),
    );
  }

  void onTap(int index, GlobalKey<FormState> formKey) {
    if (formKey.currentState!.validate()) {
      // ENREGISTRER LES VALEURS
      Navigator.pop(context);
    }
  }

  Widget titleInput(TextEditingController txtTitle) {
    bool isValid = true;
    final state = GlobalKey<FormState>();

    return Padding(
      padding: const EdgeInsets.all(16),
      child: Center(
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              color: secondaryColor),
          child: TextFormField(
            controller: txtTitle,
            textAlign: TextAlign.center,
            decoration: InputDecoration(
                hintText: 'Titre de la recette',
                border: InputBorder.none,
                focusedBorder: InputBorder.none),
          ),
        ),
      ),
    );
  }

  Widget desciptionInput() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
          child: Text(
            'Description: ',
            style: TextStyle(color: primaryColor, fontSize: 18),
          ),
        ),
        Container(
          decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: secondaryColor),
          child: TextFormField(
            maxLines: 10,
          ),
        )
      ],
    );
  }

  Widget imageInput() {
    return Text('image');
  }

  List<Widget> ingredientsInput() {
    List<Widget> widgets = [];
    widgets.add(
      const Padding(
        padding: EdgeInsets.all(8.0),
        child: Text(
          'Ingredients:',
          style: TextStyle(color: primaryColor, fontSize: 20),
        ),
      ),
    );
    widgets.addAll(
      _ingredients.map(
        (ingredient) => Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: FutureBuilder<String>(
                  future: getIngredientText(ingredient),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      return Text('${snapshot.data}');
                    }
                    return Text('donnée en cours de chargement');
                  },
                ),
              ),
              SizedBox(
                height: 25,
                width: 25,
                child: IconButton(
                    padding: EdgeInsets.all(0),
                    iconSize: 25,
                    splashRadius: 25,
                    onPressed: () {
                      setState(() {
                        _ingredients.remove(ingredient);
                      });
                    },
                    icon: Icon(Icons.remove_circle_outline)),
              ),
            ],
          ),
        ),
      ),
    );
    widgets.add(
      Padding(
          padding: EdgeInsets.fromLTRB(8, 0, 0, 8),
          child: Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                SizedBox(
                  height: 25,
                  width: 25,
                  child: IconButton(
                      padding: EdgeInsets.all(0),
                      iconSize: 25,
                      splashRadius: 25,
                      onPressed: addIngredients,
                      icon: Icon(Icons.add_circle_outline)),
                ),
              ],
            ),
          )),
    );
    return widgets;
  }

  void addIngredients() async {
    String? _unity;
    List<String> _unities = ['', 'g', 'kg', 'm'];
    final _formKey = GlobalKey<FormState>();
    showDialog(
      context: context,
      builder: (context) {
        return CustomDialog(
          title: Container(
            decoration: const BoxDecoration(
              color: primaryColor,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20)),
            ),
            child: const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                'Ajouter un ingrédient',
                style: TextStyle(color: secondaryColor),
              ),
            ),
          ),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
                  Padding(
                    padding: EdgeInsets.fromLTRB(5, 5, 30, 5),
                    child: TextFormField(
                      controller: txtQuantity,
                      decoration: InputDecoration(
                        hintText: 'quantité',
                        constraints: BoxConstraints(maxWidth: 60),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(30, 5, 5, 5),
                    child: DropdownButtonFormField(
                      value: _unity,
                      decoration: InputDecoration(
                        constraints: BoxConstraints(maxWidth: 70),
                      ),
                      hint: Text('unité'),
                      items: _unities.map((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      onChanged: (value) {
                        setState(() {
                          _unity = value ?? '';
                        });
                      },
                    ),
                  ),
                ]),
                Padding(
                  padding: EdgeInsets.all(5.0),
                  child: TextFormField(
                    decoration:
                        InputDecoration(hintText: "Nom de l'ingredient"),
                    controller: txtIngredientName,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(5.0),
                  child: TextFormField(
                    decoration:
                        InputDecoration(hintText: "Catégorie de l'ingredient"),
                    controller: txtIngredientcategory,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: ElevatedButton(
                    onPressed: () {
                      saveIngredient(_unity);
                    },
                    child: Text('valider'),
                  ),
                )
              ],
            ),
          ),
        );
      },
    ).then(
      (value) {
        if (value != null) {
          setState(() {
            _ingredients.add(value);
          });
        }
      },
    );
  }

  Widget scoreInput() {
    return RatingBar.builder(
      initialRating: _initialRating,
      minRating: 1,
      allowHalfRating: true,
      unratedColor: Colors.amber.withAlpha(50),
      itemCount: 5,
      itemSize: 25.0,
      itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
      itemBuilder: (context, _) => const Icon(
        Icons.star,
        color: Colors.amber,
      ),
      onRatingUpdate: (rating) {
        setState(() {
          _rating = rating;
        });
      },
      updateOnDrag: true,
    );
  }

  Future<String> getIngredientText(IngredientsRecipes ingredientRecipe) async {
    Ingredient ingredient =
        await ingredientDAO.fetchIngredientById(ingredientRecipe.id_ingredient);
    return '${ingredientRecipe.quantity} ${ingredientRecipe.unity ?? ''} ${ingredient.name}';
  }

  saveIngredient(unity) {
    Ingredient ingredient = Ingredient(
        name: txtIngredientName.text, category: txtIngredientcategory.text);
    ingredientDAO.upsertIngredient(ingredient).then((ingredient) {
      IngredientsRecipes result = IngredientsRecipes(
          id_ingredient: ingredient.id!,
          quantity: int.parse(txtQuantity.text),
          unity: unity);
      Navigator.pop(context, result);
    });
  }
}
