import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/widgets.dart';

import '../../database/operations/recipeDAO.dart';
import '../../models/recipe.dart';

class RecipeWidget extends StatefulWidget {
  const RecipeWidget({
    super.key,
    required this.id_recipe,
  });

  final int id_recipe;
  static const String DEFAULT_IMAGE = "";

  @override
  State<RecipeWidget> createState() => _RecipeWidgetState();
}

class _RecipeWidgetState extends State<RecipeWidget> {
  @override
  Widget build(BuildContext context) {
    RecipeDAO recipeDAO = RecipeDAO();
    late Recipe recipe;
    recipeDAO.getRecipe(this.widget.id_recipe).then((value) => recipe = value);
    // recipeId
    // image
    // test
    // tags
    return Center(
        child: Card(
      child: InkWell(
        onTap: () {},
      ),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
    ));
  }

  Widget? getTags() {
    return null;
  }
}
