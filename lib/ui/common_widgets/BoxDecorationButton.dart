import 'package:flutter/material.dart';
import 'package:qu_est_ce_que_on_mange/ui/styles/Colors.dart';

class BoxDecorationButton extends BoxDecoration {
  final BorderRadiusGeometry? borderRadius;
  final List<BoxShadow>? boxShadow;

  const BoxDecorationButton(
      {this.borderRadius = const BorderRadius.all(Radius.circular(30)),
      this.boxShadow = const [
        BoxShadow(blurRadius: 4, offset: Offset(0, 4), color: colorShadow)
      ]});
}
