import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseRepository {
  DatabaseRepository.privateConstructor();

  static final DatabaseRepository instance =
      DatabaseRepository.privateConstructor();

  final _databaseName = 'test4';
  final _databaseVersion = 1;

  static Database? _database;

  Future<Database?> get database async {
    _database ??= await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _databaseVersion,
        onCreate: await onCreate,
        onConfigure: _onConfigure);
  }

  static Future _onConfigure(Database db) async {
    await db.execute('PRAGMA foreign_keys = ON');
  }

  Future onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE menu (
          id   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT
          )
          ''');

    await db.execute('''
          CREATE TABLE ingredients_recipe (
          id_ingredient   INTEGER NOT NULL ,
          id_recipe   INTEGER NOT NULL,
          quantity INTEGER,
          unity TEXT,

          PRIMARY KEY (id_ingredient, id_recipe)
          FOREIGN KEY (id_ingredient) REFERENCES ingredient (id)
          FOREIGN KEY (id_recipe) REFERENCES recipe (id)
          )
          ''');

    await db.execute('''
          CREATE TABLE ingredient (
          id   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
          name TEXT    NOT NULL,
          category TEXT NOT NULL
          )     
          ''');

    await db.execute('''
          CREATE TABLE ingredients_shopping (
          id_ingredient   INTEGER NOT NULL,
          id_shopping   INTEGER NOT NULL,
          quantity TEXT,
          unity TEXT,

          PRIMARY KEY(id_ingredient,id_shopping)
          FOREIGN KEY (id_ingredient) REFERENCES ingredient (id)
          FOREIGN KEY (id_shopping) REFERENCES shopping (id)
          )
          ''');

    await db.execute('''
          CREATE TABLE recipe (
          id   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
          id_menu INTEGER NOT NULL REFERENCES menu (id),
          title TEXT,
          description TEXT,
          image TEXT,
          score INTEGER
          )
          ''');

    await db.execute('''
          CREATE TABLE shopping (
          id   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
          date_shopping TEXT
          )
          ''');

    await db.execute('''
          CREATE TABLE tag (
          id   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
          name TEXT
          )
          ''');

    await db.execute('''
          CREATE TABLE tags_recipes (
          id_recipe   INTEGER NOT NULL,
          id_tag   INTEGER NOT NULL,

          PRIMARY KEY (id_recipe, id_tag)
          FOREIGN KEY (id_recipe) REFERENCES recipe (id)
          FOREIGN KEY (id_tag) REFERENCES tag (id)
          )
          ''');
  }
}
