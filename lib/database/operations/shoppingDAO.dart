import '../../models/shopping.dart';
import '../database.dart';

class ShoppingDAO {
  final dbProvider = DatabaseRepository.instance;

  createMenu(Shopping shopping) async {
    final db = await dbProvider.database;
    db!.insert('shopping', shopping.toMap());
  }
}
