import '../../models/tag.dart';
import '../database.dart';

class TagDAO {
  final dbProvider = DatabaseRepository.instance;

  createMenu(Tag tag) async {
    final db = await dbProvider.database;
    db!.insert('tag', tag.toMap());
  }
}
