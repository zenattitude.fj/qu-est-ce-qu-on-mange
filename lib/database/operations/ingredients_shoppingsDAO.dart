import '../../models/ingredients_shoppings.dart';
import '../database.dart';

class IngredientsShoppingDAO {
  final dbProvider = DatabaseRepository.instance;

  createMenu(IngredientsShoppings ingredientsShoppings) async {
    final db = await dbProvider.database;
    db!.insert('ingredients_shoppings', ingredientsShoppings.toMap());
  }
}
