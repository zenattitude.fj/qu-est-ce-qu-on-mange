import 'package:sqflite/sqflite.dart';

import '../../models/ingredient.dart';
import '../database.dart';

class IngredientDAO {
  final dbProvider = DatabaseRepository.instance;

  Future<Ingredient> fetchIngredientById(int id_ingredient) async {
    final db = await dbProvider.database;
    List<Map<String, dynamic>> results = await db!.query(
      Ingredient.TABLE_NAME,
      where: "id = ?",
      whereArgs: [id_ingredient],
    );
    Ingredient ingredient = Ingredient.fromMap(results[0]);
    return ingredient;
  }

  Future<Ingredient> upsertIngredient(Ingredient ingredient) async {
    final db = await dbProvider.database;
    var id;
    var count = Sqflite.firstIntValue(await db!.rawQuery(
        "SELECT COUNT(*) FROM ingredient WHERE id = ?", [ingredient.id]));
    if (count == 0) {
      id = await db.insert("ingredient", ingredient.toMap(),
          conflictAlgorithm: ConflictAlgorithm.replace);
    } else {
      id = await db.update("ingredient", ingredient.toMap(),
          where: "id = ?", whereArgs: [ingredient.id]);
    }
    return ingredient.withId(id);
  }
}
