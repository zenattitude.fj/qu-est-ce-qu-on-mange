import '../../models/tags_recipes.dart';
import '../database.dart';

class TagsRecipesDAO {
  final dbProvider = DatabaseRepository.instance;

  createMenu(TagsRecipes tagsRecipes) async {
    final db = await dbProvider.database;
    db!.insert('tags_recipes', tagsRecipes.toMap());
  }
}
