import 'package:sqflite/sqflite.dart';

import '../../models/recipe.dart';
import '../database.dart';

class RecipeDAO {
  final dbProvider = DatabaseRepository.instance;

  createRecipe(Recipe recipe) async {
    final db = await dbProvider.database;
    db!.insert(Recipe.TABLE_NAME, recipe.toMap(),
        nullColumnHack: 'id', conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<List<Recipe>> getAllRecipe() async {
    final db = await dbProvider.database;
    List<Map<String, dynamic>> results = await db!.query(Recipe.TABLE_NAME);
    List<Recipe> recipes = [];

    results.forEach((result) {
      Recipe recipe = Recipe.fromMap(result);
      recipes.add(recipe);
    });
    return recipes;
  }

  Future<Recipe> getRecipe(int id_recipe) async {
    final db = await dbProvider.database;
    List<Map<String, dynamic>> results = await db!.query(
      Recipe.TABLE_NAME,
      where: "id = ?",
      whereArgs: [id_recipe],
    );
    Recipe recipe = Recipe.fromMap(results[0]);
    return recipe;
  }
}
