import 'package:sqflite/sql.dart';

import '../../models/menu.dart';
import '../database.dart';

class MenuDAO {
  final dbProvider = DatabaseRepository.instance;

  createMenu(Menu menu) async {
    final db = await dbProvider.database;
    db!.insert('menu', menu.toMap(),
        nullColumnHack: 'id', conflictAlgorithm: ConflictAlgorithm.replace);
  }
}
