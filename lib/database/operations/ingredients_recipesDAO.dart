

import '../../models/ingredients_recipes.dart';
import '../database.dart';

class IngredientsRecipesDAO {
  final dbProvider = DatabaseRepository.instance;

  createMenu(IngredientsRecipes ingredientsRecipes) async {
    final db = await dbProvider.database;
    db!.insert('ingredients_recipes', ingredientsRecipes.toMap());
  }
}
